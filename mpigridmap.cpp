/**
 * mpigridmap.cpp: GRID mapping algorithms.
 *                 Realises a multilevel partition in the multicluster systems:
 *                 1. Partition by the clusters.
 *                 2. Within each cluster-partition partition by the nodes.
 *
 * (C) 2012 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <map>
#include <sstream>

#include <stdlib.h>
#include <string.h>

#include "gpart.h"

using namespace std;

const int   PPN = 8;                 // Standart number of CPUs per node.

// Computer node
class node_t
{
public:
    string cluster;     // Cluster name (xeon16, xeon32, etc)
    string name;        // Node name (node1, node2, etc)
    int ppn;            // Total CPUs per node
    int ppn_curr;       // Current available number of CPUs
    int num;            // No. of the node

    node_t(const int _num): num(_num) {}
    node_t() {}
    bool operator==(const node_t &n) 
        { return (n.num == num) && (ppn_curr < ppn); }
};

class cluster_t 
{ 
public:
    int num;                // Cluster number
    int nprocs;             // Number of processors in the cluster
    int ppn;                // Processors per node in the cluster
    string name;            // Cluster name

    cluster_t() {}
    cluster_t(const string cl_name): name(cl_name) {}
    bool operator==(const cluster_t &cl) 
        { return cl.name == name; }
    bool operator<(const cluster_t &cl) const
        { return cl.nprocs < nprocs; }
};

// Rankfile or map of the ranks to the nodes
class rankmap_t 
{
public:
    int rank;           // Vertex (MPI process)
    string node;        // Node name
    string cluster;     // Cluster name
    int slot;           // Slot number within the node

    friend ostream& operator<< (ostream &out, rankmap_t &rankmap_item)
    {
        out << "rank "  << rankmap_item.rank << "=" << rankmap_item.node
            << " slot=" << rankmap_item.slot << endl;
        return out;
    }

    bool operator<(const rankmap_t &rm) const
        { return rm.rank > rank; }
};

// checkusage: Check cmd line. 
void checkusage(int argc, char **argv, string &csr, string &machinefile,
                string &rankfile);

// readmfile: Read info about all the nodes.
void readmfile(string machinefile, vector<node_t> &nodes);

// partcsr: Part CSR graph.
void partcsr(string csr, vector<node_t> nodes, string rankfile);

// part_by_clusters: Part graph by number of nodes in the clusters.
void part_by_clusters(csrgraph_t *graph, vector<cluster_t> &clusters,
                      vector<node_t> &nodes, vector<rankmap_t> &rankmap);

//
// part_by_nodes: Part each cluster-partition by nodes within that cluster.
//                graph - base (full) taskgraph,
//                rankmap - base rankmap (after the 1st partition),
//                newrankmap - result rankmap (after the 2nd partition)
//
void part_by_nodes(csrgraph_t *graph, vector<cluster_t> &clusters, 
                   vector<node_t> &nodes, vector<rankmap_t> &rankmap,
                   vector<rankmap_t> &newrankmap);

//
// genrankmap: Generate rankmap from the partition.
//             nodes - list of the cluster's nodes,
//             nranks - number of ranks (CPU cores) in the cluster,
//             part - partition array,
//             newranks_reverse - cluster to multicluster ranks mapping, 
//             rankmap_ix - left bound in the rankmap (place for that cluster).
//
void genrankmap(vector<node_t> &nodes, int nranks, int *part, 
                map<int, int> &newranks_reverse, int &rankmap_ix, 
                vector<rankmap_t> &rankmap);

// writerankfile: Write rankmap to the rankfile.
void writerankfile(vector<rankmap_t> &rankmap, string rankfile);

//
// assign_nodenums_by_node: Assign numbers to the nodes. 
//                          Partition unit is the node.
//                          
void assign_nodenums_by_node(vector<node_t> &nodes, int base_ppn);

//
// assign_nodenums_by_clusters: Assign numbers to the nodes. 
//                              Partition unit is a cluster.
//
void assign_nodenums_by_clusters(vector<node_t> &nodes, 
                                 vector<cluster_t> &clusters);

//
// clusters_sort_nodes_reassign: Sort clusters in decreasing order of nprocs.
//                               Reassign nodes numbers to the nodes.
//
void clusters_sort_nodes_reassign(vector<cluster_t> &clusters, 
                                  vector<node_t> &nodes);

int main(int argc, char **argv)
{
    vector<node_t> nodes;
    map<string, int> clusters;  // <cluster_name, cluster_no>
    vector<int> part;
    string csr, machinefile, rankfile;

    try {
        checkusage(argc, argv, csr, machinefile, rankfile);
        readmfile(machinefile, nodes);
        partcsr(csr, nodes, rankfile);
    }
    
    catch (const char *err) {
        cout << "An exception: " << err << endl;
    }

    return 0;
}

// checkusage: Check cmd line. 
void checkusage(int argc, char **argv, string &csr, string &machinefile,
                string &rankfile)
{
    if (argc != 4)
        goto err;

    csr = argv[1];
    machinefile = argv[2];
    rankfile = argv[3];
    return;

err:
    cout << "Usage: " << argv[0] << " csr machinefile rankfile\n";
    throw "cmd line error";
}

// readmfile: Read info about all the nodes.
void readmfile(string machinefile, vector<node_t> &nodes)
{
    ifstream mfile(machinefile.c_str());
    string slots;
    node_t node;

    if (!mfile)
        throw "bad machinefile";

    // Read nodes from file, fill the fields.
    while ((mfile >> node.name) && (mfile >> slots)) {
        int pos = node.name.find("-");
        node.cluster.assign(node.name, 0, pos);

        pos = slots.find('=');
        string nstr;
        nstr.assign(slots, pos + 1, slots.length());
        node.ppn = atoi(nstr.c_str());
        node.ppn_curr = 0;

        nodes.push_back(node);
    }

    mfile.close();
}

// partcsr: Part CSR graph.
void partcsr(string csr, vector<node_t> nodes, string rankfile)
{
    csrgraph_t *graph;                  // Task graph.
    vector<cluster_t> clusters;         // All the clusters.

    //
    // 1. Part by clusters.
    // 2. Part each cluster-part by the nodes within that cluster.
    //

    // Load graph from CSR-file.
    if ((graph = csrgraph_load(csr.c_str())) == NULL) 
        throw "can't load graph file";

    /*
    cout << "nv = " << graph->nvertices << endl;
    for (int i = 0; i < graph->nvertices; i++) 
        printf("adjix[%d] = %d\n", i, graph->adjindexes[i]);
    for (int i = 0; i < graph->nedges; i++) 
        printf("edges[%d] = %d\n", i, graph->edges[i]);
    for (int i = 0; i < graph->nedges; i++)
        printf("vert[%d] = %d\n", i, graph->adjv[i]);
    //exit(0);
    */
    
    // Map of ranks to the nodes (rankfile).
    // For the 1st (cluster) partition 
    vector<rankmap_t> rankmap1(graph->nvertices);        
    // For the 2nd partition
    vector<rankmap_t> rankmap2(graph->nvertices);     

    // Partition by clusters
    part_by_clusters(graph, clusters, nodes, rankmap1);

    // TODO: 
    // Add possibility of choosing the way of mapping (by cluster or by both
    // clusters and nodes) by argumetns.
    //writerankfile(rankmap1, "rf1");

    // Partition by the nodes
    part_by_nodes(graph, clusters, nodes, rankmap1, rankmap2);

    // Write to the file.
    writerankfile(rankmap2, rankfile);

    csrgraph_free(graph);
}

// part_by_clusters: Part graph by number of nodes in the clusters.
void part_by_clusters(csrgraph_t *graph, vector<cluster_t> &clusters,
                      vector<node_t> &nodes, vector<rankmap_t> &rankmap)
{
    int *part;                          // Graph partitions.

    if ((part = (int *) malloc(sizeof(*part) * graph->nvertices + 1)) == NULL) 
        throw "malloc() failed for part";

    // Assign node numbers by clusters.
    assign_nodenums_by_clusters(nodes, clusters);
    
    // FIXME: This sort and nodes number reassign is for correct partition 
    //        using gpart.
    clusters_sort_nodes_reassign(clusters, nodes);

    int nparts = clusters.size();   // Number of clusters
    int *pweights;                  // Number of nodes in the clusters.

    if ((pweights = (int *)malloc(sizeof(*pweights) * nparts)) == NULL) 
        throw "malloc() failed for pweight";

    // Count sum of nodes in clusters and fill weights of partitions.
    int cl_nprocs_sum = 0, i = 0;
    vector<cluster_t>::iterator cl_iter;
    for (cl_iter = clusters.begin(); cl_iter != clusters.end(); cl_iter++) {
        //cout << cl_iter->name << " " << cl_iter->nprocs << endl;
        cl_nprocs_sum += cl_iter->nprocs;
        pweights[i] = cl_iter->nprocs;
        //cout << "pw " << pweights[i] << endl;
        i++;
    }

    /*
    // FIXME
    // Only with these values it works.
    if (graph->nvertices == 120) {
        pweights[0] = 16;
        pweights[1] = 96;
        pweights[2] = 8;
    }
    */

    // Sum of nodes in machinefile must be equal number of vertices in the graph
    if (graph->nvertices != cl_nprocs_sum) {
        stringstream err;
        err << "(graph->nvertices = "  << graph->nvertices 
            << ") != (cl_nprocs_sum = " << cl_nprocs_sum << ")";
        throw err.str().c_str();
    }

    // Do graph partition.
    //cout << "gpart(" << graph->nvertices << " " << nparts << ")\n";
    if (gpart_partition_recursive(graph, pweights, nparts, part) > 0) 
        throw "error partitioning graph";

    /*
    for (i = 0; i < graph->nvertices; i++) {
        cout << "part " << i << " = " << part[i] << endl;
    }
    */

    // Generate rankfile from the partition.
    vector<node_t>::iterator n_it;
    //rankmap_t new_rankmap;
    int rankmap_ix = 0;
    for (int rank = 0; rank < graph->nvertices; rank++) {

        /*
        cout << "=============================================\n";
        cout << "look for " << part[rank] << endl;
        vector<node_t>::iterator it;
        for (it = nodes.begin(); it != nodes.end(); it++) {
            cout << it->num << " " << it->name << " " << it->ppn_curr << endl;
        }
        */

        // Find node with current index part[rank]
        n_it = find(nodes.begin(), nodes.end(), node_t(part[rank]));
        if (n_it == nodes.end()) 
            throw "invalid partition: can't find free slots";

        rankmap[rankmap_ix].rank = rank;
        rankmap[rankmap_ix].node = n_it->name;
        rankmap[rankmap_ix].slot = n_it->ppn_curr;
        rankmap[rankmap_ix].cluster = n_it->cluster;

        //cout << rankmap[rankmap_ix];

        rankmap_ix++;

        n_it->ppn_curr++;
    }

    free(pweights);
    free(part);
}

//bool isincluster(string cluster) { return cluster == 

//
// part_by_nodes: Part each cluster-partition by nodes within that cluster.
//                graph - base (full) taskgraph,
//                rankmap - base rankmap (after the 1st partition),
//                newrankmap - result rankmap (after the 2nd partition)
//
void part_by_nodes(csrgraph_t *graph, vector<cluster_t> &clusters, 
                   vector<node_t> &nodes, vector<rankmap_t> &rankmap,
                   vector<rankmap_t> &newrankmap)
{
    vector<cluster_t>::iterator cl_it;
    vector<rankmap_t>::iterator r_it;
    csrgraph_t *subgraph = NULL;            // Task graph for each cluster.
    int rankmap_ix;                         // Left index in the rankmap
                                            // for each cluster.
    for (cl_it = clusters.begin(); cl_it != clusters.end(); cl_it++) {
        //
        // For each of the cluster do the partition by its nodes.
        // 1. Count number of nodes and edges in each partition.
        // 3. Compute new ranks for the cluster.
        // 2. Create subgraph for the cluster from the multicluster.
        // 4. Do partition within the cluster.
        // 5. Generate the part of rankmap corresponding to the cluster.
        //

        // Count number of edges incident to the vertices containing 
        // in the current cluster-partition.
        int nedges = 0;             
        for (r_it = rankmap.begin(); r_it != rankmap.end(); r_it++) 
            if (r_it->cluster == cl_it->name) 
                nedges += graph->adjindexes[r_it->rank + 1] -
                          graph->adjindexes[r_it->rank];

        // Create subgraph for the partition.
        subgraph = csrgraph_create(cl_it->nprocs, nedges);
        if (subgraph == NULL)
            throw "can't create subgraph for the cluster";
        
        // Find nodes containing in that cluster.
        vector<node_t> cluster_nodes;
        vector<node_t>::iterator n_it;
        cluster_nodes.clear();
        int cl_proc_sum = 0;
        for (n_it = nodes.begin(); n_it != nodes.end(); n_it++) 
            if (n_it->cluster == cl_it->name) {
                n_it->ppn_curr = 0;
                cluster_nodes.push_back(*n_it);
                cl_proc_sum += n_it->ppn;
            }

        // Compute new ranks for the clusters.
        map<int, int> newranks;         // multicluster -> cluster
        map<int, int> newranks_reverse; // cluster -> multicluster
        int newrank = 0;
        for (r_it = rankmap.begin(); r_it != rankmap.end(); r_it++) 
            if (r_it->cluster == cl_it->name) {
                newranks.insert(pair<int, int>(r_it->rank, newrank));
                newranks_reverse.insert(pair<int, int>(newrank, r_it->rank));
                newrank++;
            }

        // Form a new graph from the full one.
        int v = 0, rank = 0;
        for (r_it = rankmap.begin(); r_it != rankmap.end(); r_it++) {
            if (r_it->cluster == cl_it->name) {
                bool isfirstedge = true;
                for (int j = graph->adjindexes[r_it->rank]; 
                         j < graph->adjindexes[r_it->rank + 1]; j++) {

                    if (rankmap[graph->adjv[j]].cluster == cl_it->name) {
                        // If rank in that edge communicate with the other rank
                        // in that cluster.

                        if (isfirstedge) {
                            // It's the first incident edge for that vertex.
                            subgraph->adjindexes[rank] = v;
                            isfirstedge = false;
                        }

                        //subgraph->adjv[v] = graph->adjv[j];
                        subgraph->adjv[v] = newranks[graph->adjv[j]];
                        subgraph->edges[v] = graph->edges[j];

                        /*
                        // Debug
                        cout << "rank " << rank << " ";
                        cout << "edge " << v 
                             << " " << subgraph->adjv[v] << " "
                             << subgraph->edges[v] << endl;
                        */
                        v++;
                    }
                }
                rank++;
            }
        }
        subgraph->nvertices = rank;
        subgraph->nedges = v;
        // Right bound of incident indexes for the last rank.
        subgraph->adjindexes[rank] = v; 

        /*
        // Debug
        // Write subgraph to file
        ofstream subgraph_file("subgraph");
        subgraph_file << rank << " " << v / 2 << " 1" << endl;
        for (int iv = 0; iv < subgraph->nvertices; iv++) {
            cout << iv << " from " << subgraph->adjindexes[iv] 
                 << " to " << subgraph->adjindexes[iv + 1] << endl;
            for (int ie = subgraph->adjindexes[iv]; 
                     ie < subgraph->adjindexes[iv + 1]; ie++) {
                subgraph_file << subgraph->adjv[ie] + 1 << " " 
                              << subgraph->edges[ie] << " ";
            }
            subgraph_file << endl;
        }
        subgraph_file.close();
        */

        // Fill partitions' weights.
        int nparts = cluster_nodes.size();

        int *part;                      // Graph partitions.
        if ((part = (int *) malloc(sizeof(*part) * cl_proc_sum)) == NULL) 
            throw "malloc() failed for part";

        int *pweights;                  // Numbers of processors in the clusters
        if ((pweights = (int *)malloc(sizeof(*pweights) * nparts)) == NULL) 
            throw "malloc() failed for pweight";

        // Debug
        for (int i = 0; i < nparts; i++) 
            pweights[i] = cl_it->ppn;

        /*
        // Do graph partition.
        // Debug
        cout << "part " << nparts << " " << endl;
        cout << "nv " << subgraph->nvertices << endl;
        cout << "ne " << subgraph->nedges << endl;

        cout << "nv = " << subgraph->nvertices << endl;
        for (int i = 0; i < subgraph->nvertices; i++) 
            printf("%s-1 adjix[%d] = %d\n", cl_it->name.c_str(), 
                    i, subgraph->adjindexes[i]);
        for (int i = 0; i < subgraph->nedges; i++) 
            printf("%s-1 edges[%d] = %d\n", cl_it->name.c_str(), 
                    i, subgraph->edges[i]);
        for (int i = 0; i < subgraph->nedges; i++)
            printf("%s-1 vert[%d] = %d\n", cl_it->name.c_str(), 
                    i, subgraph->adjv[i]);
        */

        /*
        csrgraph_t *g = NULL;
        if ((g = csrgraph_load("subgraph")) == NULL) 
            throw "can't load graph file";

        // Debug
        cout << "========================================\n";
        cout << "part " << nparts << " " << endl;
        cout << "nv " << g->nvertices << endl;
        cout << "ne " << g->nedges << endl;
        for (int i = 0; i < g->nvertices; i++) 
            printf("%s-2 adjix[%d] = %d\n", cl_it->name.c_str(), 
                    i, g->adjindexes[i]);
        for (int i = 0; i < g->nedges; i++) 
            printf("%s-2 edges[%d] = %d\n", cl_it->name.c_str(), 
                    i, g->edges[i]);
        for (int i = 0; i < g->nedges; i++)
            printf("%s-2 vert[%d] = %d\n", cl_it->name.c_str(), 
                    i, g->adjv[i]);

        if (gpart_partition_recursive(g, pweights, nparts, part) > 0) 
            throw "error partitioning graph";

        csrgraph_free(g);
        */

        if ((gpart_partition_recursive(subgraph, pweights, nparts, part)) > 0)
            throw "error partitioning graph";

        /*
        // Debug
        cout << "part for " << cl_it->name << endl;
        for (int i = 0; i < cl_proc_sum; i++) 
            cout << "part[" << i << "] = " << part[i] << endl;
        */

        // Enumerate cluster's nodes
        assign_nodenums_by_node(cluster_nodes, cl_it->ppn);

        /*
        // Debug
        for (n_it = cluster_nodes.begin(); n_it != cluster_nodes.end(); n_it++)
            cout << n_it->cluster << " " << n_it->name << " "
                 << n_it->num << endl;
        */

        // Generate rankfile from the partition.
        genrankmap(cluster_nodes, cl_proc_sum, part, newranks_reverse, 
                   rankmap_ix, newrankmap);

        csrgraph_free(subgraph);

        free(pweights);
        free(part);

        subgraph = NULL;
    }

    // Sort rankmap by ranks.
    sort(newrankmap.begin(), newrankmap.end());
}

// writerankfile: Write rankmap to the rankfile.
void writerankfile(vector<rankmap_t> &rankmap, string rankfile)
{
    ofstream rf(rankfile.c_str());
    if (!rf)
        throw "can't open rankfile";

    vector<rankmap_t>::iterator r_it;
    for (r_it = rankmap.begin(); r_it != rankmap.end(); r_it++) 
        rf << *r_it;

    rf.close();
}

//
// genrankmap: Generate rankmap from the partition.
//             nodes - list of the cluster's nodes,
//             nranks - number of ranks (CPU cores) in the cluster,
//             part - partition array,
//             newranks_reverse - cluster to multicluster ranks mapping, 
//             rankmap_ix - left bound in the rankmap (place for that cluster).
//
void genrankmap(vector<node_t> &nodes, int nranks, int *part, 
                map<int, int> &newranks_reverse, int &rankmap_ix, 
                vector<rankmap_t> &rankmap)
{
    vector<node_t>::iterator n_it;

    for (int rank = 0; rank < nranks; rank++) {
        // Find node with current index part[rank]
        n_it = find(nodes.begin(), nodes.end(), node_t(part[rank]));
        if (n_it == nodes.end()) 
            throw "invalid partition";

        //rankmap[rankmap_ix].rank = rankmap_ix;
        rankmap[rankmap_ix].rank = newranks_reverse[rank];
        rankmap[rankmap_ix].node = n_it->name;
        rankmap[rankmap_ix].slot = n_it->ppn_curr;
        rankmap[rankmap_ix].cluster = n_it->cluster;

        //cout << rankmap[rankmap_ix];
        rankmap_ix++;
        n_it->ppn_curr++;
    }
}

//
// assign_nodenums_by_node: Assign numbers to the nodes. 
//                          Partition unit is the node.
//
void assign_nodenums_by_node(vector<node_t> &nodes, int base_ppn)
{
    // Assigne numbers to the nodes.
    vector<node_t>::iterator iter;
    int ppn_acc = 0, node_num = 0;
    for (iter = nodes.begin(); iter != nodes.end(); iter++) {
        iter->num = node_num;
        ppn_acc += iter->ppn;
        if (ppn_acc >= base_ppn) {
            node_num++;
            ppn_acc = 0;
        }
    }
}

//
// assign_nodenums_by_clusters: Assign numbers to the nodes. 
//                              Partition unit is a cluster.
//
void assign_nodenums_by_clusters(vector<node_t> &nodes, 
                                 vector<cluster_t> &clusters)
{
    int cl_num = 0;
    vector<node_t>::iterator n_it;
    vector<cluster_t>::iterator find_cl;
    cluster_t cl_new;
    cl_new.nprocs = 0;

    for (n_it = nodes.begin(); n_it != nodes.end(); n_it++) {
        find_cl = find(clusters.begin(), clusters.end(), 
                       cluster_t(n_it->cluster));

        if (find_cl == clusters.end()) {
            cl_new.name = n_it->cluster;
            cl_new.num = cl_num;
            cl_new.nprocs = n_it->ppn;
            cl_new.ppn = n_it->ppn;
            n_it->num = cl_num;
            clusters.push_back(cl_new);
            cl_num++;
        } else {
            n_it->num = find_cl->num;
            find_cl->nprocs += n_it->ppn;
        }
    }
}

//
// FIXME: This is for partition working only
// clusters_sort_nodes_reassign: Sort clusters in decreasing order of nnodes.
//                               Reassign nodes numbers to the nodes.
//
void clusters_sort_nodes_reassign(vector<cluster_t> &clusters, 
                                  vector<node_t> &nodes)
{
    sort(clusters.begin(), clusters.end());
    vector<cluster_t>::iterator _cl_it;
    int cl_num = 0;
    for (_cl_it = clusters.begin(); _cl_it != clusters.end(); _cl_it++) {
        _cl_it->num = cl_num;
        cl_num++;
    }

    vector<node_t>::iterator _n_it;
    vector<cluster_t>::iterator find_cl;
    for (_n_it = nodes.begin(); _n_it != nodes.end(); _n_it++) {
        find_cl = find(clusters.begin(), clusters.end(), 
                       cluster_t(_n_it->cluster));
        _n_it->num = find_cl->num;
    }
}
