/*
 * example_part.c: 
 *
 * (C) 2011 Mikhail Kurnosov <mkurnosov@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>

#include "gpart.h"

//#define _DEBUG

int main(int argc, char **argv)
{
    csrgraph_t *graph;
    int *pweights, *part;
    int i, nparts;

    if (argc < 3) {
        fprintf(stderr, 
                "Usage: example_part <graphfile> <nparts> [partweights]\n");
        exit(EXIT_FAILURE);
    }
    nparts = atoi(argv[2]);    

    if ( (graph = csrgraph_load(argv[1])) == NULL) {
        fprintf(stderr, "Can not load graph file\n");
        exit(EXIT_FAILURE);
    }

#ifdef _DEBUG
    for (i = 0; i < graph->nedges; i++) {
        printf("edges[%d] = %d\n", i, graph->edges[i]);
    }
    for (i = 0; i < graph->nedges; i++) {
        printf("vert[%d] = %d\n", i, graph->adjv[i]);
    }
#endif 

    if ( (part = malloc(sizeof(*part) * graph->nvertices)) == NULL) {
        fprintf(stderr, "No enough memory\n");
        exit(EXIT_FAILURE);
    }
    if ( (pweights = malloc(sizeof(*pweights) * nparts)) == NULL) {
        fprintf(stderr, "No enough memory\n");
        exit(EXIT_FAILURE);
    }

    if (argc == 3) {
        // Only number of parts specified.
        /* pweights[i] - number of required vertices in partition i */
        for (i = 0; i < nparts; i++)
            pweights[i] = graph->nvertices / nparts;
    } else {
        // Number of vertices in partitions are specified.
        int pw_sum = 0;

        if (argc != 3 + nparts) {
            fprintf(stderr, "invalid options!\n");
            return -1;
        }

        for (i = 0; i < nparts; i++)
            pw_sum += atoi(argv[3 + i]);

        if (pw_sum != graph->nvertices) {
            fprintf(stderr, 
              "sum of partitions weights must be equal number of vertices!\n");
            return -1;
        }

        for (i = 0; i < nparts; i++)
            pweights[i] = atoi(argv[3 + i]);
    }

    /* Partition graph */
    if (gpart_partition_recursive(graph, pweights, nparts, part) > 0) {
        fprintf(stderr, "Error partitioning graph\n");
        exit(EXIT_FAILURE);
    }

    //printf("# vertex -> subset (partition)\n");
    for (i = 0; i < graph->nvertices; i++) {
        //printf("%d %d\n", i + 1, part[i] + 1); // old!
        //printf("%d %d\n", i + 1, part[i]);
        printf("%d\n", part[i]);
    }

    csrgraph_free(graph);
    free(pweights);
    free(part);
    return EXIT_SUCCESS;
}
